﻿using EFCore.Repository.Impl;
using EFCore.Repository.Interfaces;
using EFCore.Repository.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace EFCore.Repository.ConsoleTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                #region Connection from JSON
                var builder = new ConfigurationBuilder();
                builder.AddJsonFile("appsettings.json");
                var config = builder.Build();
                string connectionString = config.GetConnectionString("DefaultConnection");

                var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
                var options = optionsBuilder
                    .UseSqlServer(connectionString)
                    .Options;

                #endregion

                var context = new ApplicationContext(options);
                using (IUnitOfWork uOfWork = new UnitOfWork(context))
                {
                    Company company = new Company
                    {
                        Name = "Apple",
                        Adress = "NewYork",
                        CO = "Bill Geiz",
                        Income = 9999999999M,
                        Phone = "+1234567890"
                    };
                    Employee employee = new Employee
                    {
                        Firstname = "John",
                        Lastname = "Smith",
                        Company_Id = 1,
                        EMail = "john.smith@gmail.gmail",
                        Salary = 83000M
                    };
                    Phone phone = new Phone
                    {
                        ModelName = "Galaxy S10",
                        Company_Id = 1,
                        Price = 1000
                    };

                    uOfWork.Company.Create(company);
                    uOfWork.Employee.Create(employee);
                    uOfWork.Phone.Create(phone);

                    uOfWork.Commit();
                    Console.WriteLine("Done!");
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message);
                else
                    Console.WriteLine("Error!!!");
            }
            Console.ReadLine();
        }
    }
}
