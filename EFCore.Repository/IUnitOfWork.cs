﻿using EFCore.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IEmployeeRepository Employee { get; }
        ICompanyRepository Company { get;}
        IPhoneRepository Phone { get; }

        void Commit();
        void RejectChanges();
        void Dispose();
    }
}
