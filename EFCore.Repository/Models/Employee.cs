﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string EMail { get; set; }
        public decimal Salary { get; set; }
        public int Company_Id { get; set; }
    }
}
