﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Models
{
    public class Phone
    {
        public int Id { get; set; }
        public string ModelName { get; set; }
        public decimal Price { get; set; }
        public int Company_Id { get; set; }
    }
}
