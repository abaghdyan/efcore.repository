﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string CO { get; set; }
        public string Phone { get; set; }
        public decimal Income { get; set; }
    }
}
