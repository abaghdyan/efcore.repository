﻿using EFCore.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Interfaces
{
    public interface ICompanyRepository : IBaseRepository<Company, int>
    {
    }
}
