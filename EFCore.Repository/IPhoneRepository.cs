﻿using EFCore.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Interfaces
{
    public interface IPhoneRepository : IBaseRepository<Phone, int>
    {
    }
}
