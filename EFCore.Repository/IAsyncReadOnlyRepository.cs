﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Repository
{
    public interface IAsyncReadOnlyRepository<TEntity>
        where TEntity : class, new()
    {
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindAsync(int id);
    }
}
