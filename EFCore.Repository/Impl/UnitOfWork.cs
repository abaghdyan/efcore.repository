﻿using EFCore.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFCore.Repository.Impl
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private Dictionary<string, object> repositories;

        private bool disposed = false;

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
            Employee = new EmployeeRepository(_dbContext);
            Company = new CompanyRepository(_dbContext);
            Phone = new PhoneRepository(_dbContext);
        }

        public IEmployeeRepository Employee { get; private set; }
        public ICompanyRepository Company { get; private set; }
        public IPhoneRepository Phone { get; private set; }

        //TODO Dictionary
        //public BaseRepository<TEntity> BaseRepository<TEntity>()
        //{
        //    if (repositories == null)
        //    {
        //        repositories = new Dictionary<string, object>();
        //    }

        //    var type = typeof(TEntity).Name;

        //    if (!repositories.ContainsKey(type))
        //    {
        //        var repositoryType = typeof(BaseRepository<>);
        //        var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _dbContext);
        //        repositories.Add(type, repositoryInstance);
        //    }
        //    return (BaseRepository<TEntity>)repositories[type];
        //}


        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void RejectChanges()
        {
            var unchanged = _dbContext.ChangeTracker.Entries()
                        .Where(e => e.State != EntityState.Unchanged);
            foreach (var entry in unchanged)
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }

        #region Dispose Region
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
