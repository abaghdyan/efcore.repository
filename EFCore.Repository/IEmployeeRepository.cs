﻿using EFCore.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Interfaces
{
    public interface IEmployeeRepository : IBaseRepository<Employee, int>
    {
        decimal CalculateSalary();
    }
}
