﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository
{
    public interface IAsyncActionRepository<TEntity>
        where TEntity : class, new()
    {
        void CreateAsync(TEntity entity);
        void CreateManyAsync(IEnumerable<TEntity> entities);
    }
}
