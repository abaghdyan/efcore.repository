﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Repository.Interfaces
{
    public interface IBaseRepository<TEntity> : IActionRepository<TEntity>, IReadOnlyRepository<TEntity>, IAsyncActionRepository<TEntity>, IAsyncReadOnlyRepository<TEntity>
        where TEntity : class, new()
    { }

    public interface IBaseRepository<TEntity, TKey> : IBaseRepository<TEntity>
        where TEntity : class, new()
    {
        TEntity Find(TKey id);
    }
}
