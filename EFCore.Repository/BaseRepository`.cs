﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using EFCore.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EFCore.Repository
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class, new()
    {
        protected readonly DbContext _context;
        protected BaseRepository(DbContext dbContext)
        {
            _context = dbContext;
        }

        public void Create(TEntity entity)
        {
            if (entity != null)
                _context.Set<TEntity>().Add(entity);
        }
        public void CreateAsync(TEntity entity)
        {
            if (entity != null)
                _context.Set<TEntity>().AddAsync(entity);
        }

        public void CreateMany(IEnumerable<TEntity> entities)
        {
            if (entities != null)
                _context.Set<TEntity>().AddRange(entities);
        }

        public void CreateManyAsync(IEnumerable<TEntity> entities)
        {
            if (entities != null)
                _context.Set<TEntity>().AddRangeAsync(entities);
        }

        public void DeleteById(int id)
        {
            var entityToRemove = Find(id);
            if (entityToRemove != null)
                _context.Set<TEntity>().Remove(entityToRemove);
        }

        public void Delete(TEntity entity)
        {
            if (entity!=null)
                _context.Set<TEntity>().Remove(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            if (entities != null)
                _context.Set<TEntity>().RemoveRange(entities);
        }
        
        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate != null)
                return _context.Set<TEntity>().Any(predicate);
            else
                return false;
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate != null)
                return await _context.Set<TEntity>().AnyAsync(predicate);
            else
                return false;
        }

        public TEntity Find(int id) => _context.Set<TEntity>().Find(id);

        public async Task<TEntity> FindAsync(int id) => await _context.Set<TEntity>().FindAsync(id);

        public IEnumerable<TEntity> GetAll() => _context.Set<TEntity>();

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
            => _context.Set<TEntity>().Where(predicate);

        public void Update(TEntity entity)
        {
            if (entity != null)
                _context.Set<TEntity>().Update(entity);
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            if (entities != null)
                _context.Set<TEntity>().UpdateRange(entities);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void SaveChangesAsync()
        {
            _context.SaveChangesAsync();
        }
    }
}
